<?php
/**
 * Биссимилла!
 * Created by PhpStorm.
 * User: Aidos Batyrkulov
 * Date: 02/03/19
 * Time: 2:47 PM
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'system/config/config.class.php';
require_once 'system/core/main/runner.class.php';

$runner = new runner();
$runner->run();
