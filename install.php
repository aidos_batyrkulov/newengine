<?php
include_once 'system/core/absDB.class.php';
include_once 'system/classes/db.class.php';
include_once 'system/config/config.class.php';

$db = db::getInstance();
$config = config::getInstance();
$sql = [];

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'sef`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'sef` (
            `id`            INT(11) UNSIGNED UNIQUE     NOT NULL        AUTO_INCREMENT,
            `link`          VARCHAR(255) UNIQUE         NOT NULL,
            `alias`         VARCHAR(255) UNIQUE         NOT NULL,
            
            PRIMARY KEY (`id`)
        )';

$tables =0;
$sql_plus = ' ENGINE = MyISAM
            DEFAULT CHARSET=utf8
            COLLATE=utf8_general_ci
                 ';
foreach ($sql as $value) {
    if (mb_strlen($value)>70) $value.= $sql_plus;
    if ($db->otherQuery($value)) $tables++;
}

echo 'Created tables '.($tables/2).' OF '.(count($sql)/2);