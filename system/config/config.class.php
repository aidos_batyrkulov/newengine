<?php
// singleton
class config {
    private static $_instance;
    private $data;

    private function __construct() {
        $doc_root = $_SERVER['DOCUMENT_ROOT'].'/';
        $this->data = [
            'doc_root'=>$doc_root,
            'dir_tpl' =>$doc_root.'system/mvc/view/tpls/',
            'dir_lang' =>$doc_root.'system/mvc/view/lang/',
            'dir_controllers' =>$doc_root.'system/mvc/controllers/',

            'abs_address'=>'http://newengine.local',
            'sef_suffix'=>'.html',

            'date_format' =>'%d.%m.%Y %H:%M:%S',

            'db_host' =>'localhost',

            'db_user' =>'root',
            'db_password' =>'220297',
            /*
            'db_user' =>'mysql',
            'db_password' =>'mysql',*/

            'db_name' =>'newe',
            'db_prefix' =>'newe_',
            'db_sq' =>'?'
        ];
    }
    private function __clone () {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __get($name) {
        return $this->data[$name];
    }
}
