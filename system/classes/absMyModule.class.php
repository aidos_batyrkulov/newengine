<?php
abstract class absMyModule extends absModule  {

    public function __construct() {
        $config = config::getInstance();
        parent::__construct(new view($config->dir_tpl));
    }

}
