<?php
abstract class controller extends absController  {
    protected $lang;

    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
        $this->lang = lang::getInstance();
    }

}
