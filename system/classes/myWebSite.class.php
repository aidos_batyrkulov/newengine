<?php
class myWebSite extends absWebSite {
    private $head;
    private $hornav;

    private $top;
    private $left;
    private $content;
    private $footer;


    private function setHead() {
        $head = new head();
        $head->title = $this->getTitle();
        $this->head = $head;
    }
    public function setHornav($hornav) {
        $this->hornav = $hornav;
    }
    private function setTop() {
        $top = new top();
        $top->txt = 'top';
        $this->top = $top;
    }
    private function setLeft() {
        $left = new left();
        $left->txt ='left';
        $this->left = $left;
    }
    public function setContent($content) {
        $this->content = $content;
    }
    private function setFooter() {
        $footer = new footer();
        $footer->txt = 'footer';
        $this->footer = $footer;
    }


    public function display() {
        $html = new html();
        $this->setHead();
        $this->setTop();
        $this->setLeft();
        $this->setFooter();

        $html->head = $this->head;
        $html->hornav = $this->hornav;
        $html->top = $this->top;
        $html->left = $this->left;
        $html->content= $this->content;
        $html->footer = $this->footer;

        return $html;
    }

}