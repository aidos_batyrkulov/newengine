<?php
class mainController extends controller {
    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
    }

    public function generateContent() {
        $this->lang->load('main');

        $this->myWebSite->setTitle('Main');
        $this->myWebSite->setMetaDes('des');
        $this->myWebSite->setMetaKey('key');

        $hornav = $this->getBaseHornav();
        $this->myWebSite->setHornav($hornav);

        $main = new main();
        $main->totalUsers=5;
        $main->totalMessages=32443;
        $this->myWebSite->setContent($main);
    }
}