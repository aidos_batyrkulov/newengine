<?php
class notFound404Controller extends controller {
    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
    }

    public function generateContent() {
        $this->lang->load('404');

        $this->myWebSite->setTitle('Page not found');
        $this->myWebSite->setMetaDes('des');
        $this->myWebSite->setMetaKey('key');

        $hornav = $this->getBaseHornav();
        $this->myWebSite->setHornav($hornav);

        $main = new main();
        $main->totalUsers=5;
        $main->totalMessages=32443;
        $this->myWebSite->setContent($main);
    }
}