<!DOCTYPE html>
<html>
<?=$head?>
<body>
	<div class="container" style="margin-top: 60px;">
		<?=$top?>
		<div class="row justify-content-between">
			<div class="col-12">
				<?=$hornav?>
			</div>

			<div class="col-md-3 d-none d-md-block">
				<?=$left?>
			</div>

			<div class="col-md-8 col-12 border">
				<?=$content?>
			</div>
		</div>

		<div class="col-12 border">
			<?=$footer?>
		</div>
	</div>
</body>
</html>
