<?php

class footer extends absMyModule {
	
	public function __construct() {
		parent::__construct();
		$this->add('txt');
	}
	
	public function getTplFile() {
		return 'footer';
	}
	
}
