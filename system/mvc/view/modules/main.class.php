<?php

class main extends absMyModule {
	
	public function __construct() {
		parent::__construct();
		$this->add('totalUsers');
        $this->add('totalMessages');
	}
	
	public function getTplFile() {
		return 'main';
	}
	
}
