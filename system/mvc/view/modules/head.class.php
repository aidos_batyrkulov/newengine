<?php
class head extends absMyModule {

    public function __construct() {
        parent::__construct();
        $this->add('title');
        $this->add('styles');
        $this->add('jses');
        $this->add('metaDes');
        $this->add('metaKey');
    }

    public function getTplFile() {
        return 'head';
    }

}