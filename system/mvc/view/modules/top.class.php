<?php

class top extends absMyModule {

    public function __construct() {
        parent::__construct();
        $this->add('txt');
    }

    public function getTplFile() {
        return 'top';
    }

}
