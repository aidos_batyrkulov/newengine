<?php
class view {

    private $dir_tpl;

    public function __construct($dir_tpl) {
        $this->dir_tpl = $dir_tpl;
    }

    public function render($file, $params, $return = false) {
        $template = $this->dir_tpl.$file.".tpl";
        extract($params);
        ob_start();
        include($template);
        if ($return) return ob_get_clean();
        else echo ob_get_clean();
    }
}
