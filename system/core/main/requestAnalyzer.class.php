<?php
class requestAnalyzer {
    private static $_instance;
    private $result;

    private function __clone () {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    private function __construct() {
            $this->result='page';
    }

    public function getResult() {
        return $this->result;
    }
}