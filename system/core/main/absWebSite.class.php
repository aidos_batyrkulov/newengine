<?php
abstract class absWebSite {
    private $title;
    private $metaDes;
    private $metaKey;
    private $jses=[];
    private $styles=[];




    public function setTitle($title) {
        $this->title = $title;
    }
    public function setMetaDes($metaDes) {
        $this->metaDes = $metaDes;
    }
    public function setMetaKey($metaKey) {
        $this->metaKey = $metaKey;
    }
    public function addJs($js) {
        $this->jses[] = $js;
    }
    public function addStyle($style) {
        $this->styles[] = $style;
    }


    public function getTitle() {return $this->title;}
    public function getMetaDes() {return $this->metaDes;}
    public function getMetaKey() {return $this->metaKey;}
    public function getJses() {return $this->jses;}
    public function getStyles() {return $this->styles;}

    public abstract function setContent($content);
}