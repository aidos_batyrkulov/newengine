<?php
abstract class absController {
    protected $myWebSite;

    public function __construct(myWebSite $myWebSite){
        $this->myWebSite = $myWebSite;
    }

    public abstract function generateContent();

    protected function getBaseHornav() {
        $hornav = new hornav();
        $hornav->addData('Main');
        return $hornav;
    }

    /*
     * API methods are in controllers just for place!
     * They have not any connect with controllers!
     * They have thier own system!
     */
    public static function apiFunctionNotFound() {

    }
}