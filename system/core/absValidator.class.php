<?php

abstract class absValidator {
	protected $lang;
	protected $data;
	private $error = false;
	
	public function __construct($data) {
	    $this->lang = lang::getInstance();
		$this->data = $data;
		$this->validate();
	}
	
	abstract protected function validate();
	
	public function getError() {
		return $this->error;
	}
	
	public function isValid() {
		if ($this->error===false) return true;
		else return false;
	}
	
	protected function setError($code) {
		$this->error = $code;
	}

	
}
