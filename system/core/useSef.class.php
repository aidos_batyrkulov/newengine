<?php

class useSef {
	// link to alias
	public static function replaceSEF($link, $address = "") {
	    $config = config::getInstance();
		if ((strpos($link, "//") !== false) && (strpos($link, $config->abs_address) === false)) return $link;
		if (strpos($link, $config->abs_address) === 0) $link = substr($link, mb_strlen($config->abs_address));
		if ($link === "/") return $address.$link;
		$alias = sef::getAliasOnLink($link);
		if ($alias) $link = $address."/".$alias.$config->sef_suffix;
		else {
			$data = parse_url($link);
			$alias = sef::getAliasOnLink($data["path"]);
			if ($alias) $link = $address."/".$alias.$config->sef_suffix."?".$data["query"];
		}
		return $link;
	}

	// alias to link
	public static function getRequest($uri) {
        $config = config::getInstance();
		if (strpos($uri, $config->abs_address) !== false)
			$uri = substr($uri, strlen($config->abs_address));
		if ($uri === "/") return $uri;
		$uri = substr($uri, 1);
		$uri = str_replace($config->sef_suffix, "", $uri);
		$result = sef::getLinkOnAlias($uri);
		if (!$result) {
			$uri = substr($uri, 0, strpos($uri, "?"));
			$result = sef::getLinkOnAlias($uri);
		}
		if ($result) return $result;
		return false;
	}
	
}
